export default {
  methods: {
    showToastMixin(
      title = "",
      body = "",
      type = "success",
      customProperties = {}
    ) {
      this.$bvToast.toast(body, {
        title: title,
        variant: type,
        solid: true,
        ...customProperties
      });
    }
  }
};
