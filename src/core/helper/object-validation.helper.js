export function optionalData(object, key, optional = "-") {
  return object ? object[key] : optional;
}
