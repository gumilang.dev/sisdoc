import { app } from "../../main";

export function showToast(
  title = "",
  body = "",
  type = "success",
  customProperties = {}
) {
  app.$bvToast.toast(body, {
    title: title,
    variant: type,
    solid: true,
    ...customProperties
  });
}
