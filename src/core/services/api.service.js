import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import JwtService from "@/core/services/jwt.service";

/**
 * Service to call HTTP request via Axios
 */
const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    // Vue.axios.defaults.baseURL = "http://api.sisdoc.rumahit.co.id/v1/";
    Vue.axios.defaults.baseURL = process.env.VUE_APP_BASE_URL;
  },

  /**
   * Set the default HTTP request headers
   */
  setHeader() {
    Vue.axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${JwtService.getToken()}`;
  },

  setCors() {
    Vue.axios.defaults.headers.common["X-Requested-With"] = "localhost:8080";
  },

  setHeaderWithContent() {
    Vue.axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${JwtService.getToken()}`;
    Vue.axios.defaults.headers.common["Content-Type"] = "application/json";
  },

  setHeaderMultiPlatform() {
    Vue.axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${JwtService.getToken()}`;
    Vue.axios.defaults.headers.common["Content-Type"] = "multipart/form-data";
  },

  query(resource, params) {
    return axios.get(resource, params).catch(error => {
      throw new Error(`[KT] ApiService ${error}`);
    });
  },

  /**
   * Send the GET HTTP request
   * @param resource
   * @param slug
   * @returns {*}
   */
  get(resource, slug = "") {
    return Vue.axios.get(`${resource}/${slug}`).catch(error => {
      throw new Error(`[KT] ApiService ${error}`);
    });
  },

  /**
   * Send the GET HTTP request
   * @param resource
   * @param slug
   * @returns {*}
   */
  getDataD(resource) {
    return Vue.axios.get(`${resource}/design/jobdesk`).catch(error => {
      throw new Error(`[KT] ApiService ${error}`);
    });
  },

  /**
   * Send the GET HTTP request
   * @param resource
   * @param slug
   * @returns {*}
   */
  getDesign(resource) {
    this.setHeader();
    return Vue.axios.get(`${resource}/type`).catch(error => {
      throw new Error(`[KT] ApiService ${error}`);
    });
  },

  /**
   * Set the POST HTTP request
   * @param resource
   * @param params
   * @returns {*}
   */
  post(resource, params) {
    return Vue.axios.post(`${resource}`, params);
  },

  /**
   * Send the UPDATE HTTP request
   * @param resource
   * @param slug
   * @param params
   * @returns {IDBRequest<IDBValidKey> | Promise<void>}
   */
  update(resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params);
  },

  /**
   * Send the PUT HTTP request
   * @param resource
   * @param params
   * @returns {IDBRequest<IDBValidKey> | Promise<void>}
   */
  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  /**
   * Send the DELETE HTTP request
   * @param resource
   * @param params
   * @returns {*}
   */
  delete(resource, params = {}) {
    return Vue.axios.delete(`${resource}`, params);
  },

  /**
   * Send the GET HTTP request
   * @param resource
   * @param slug
   * @returns {*}
   */
  getCustom(resource, slug = "") {
    return Vue.axios.get(`${resource}/${slug}`);
  },

  /**
   * @param url
   * @param form
   * @param contentType
   * @param username
   * @param password
   * @param port
   * @param ftpHost
   * @param ftpType
   * @returns { Promise<AxiosResponse<any>> }
   */
  uploadToFtp(
    url,
    form,
    contentType,
    username,
    password,
    port,
    ftpHost,
    ftpType
  ) {
    return Vue.axios.post(url, form, {
      headers: {
        username,
        password,
        port,
        "ftp-host": ftpHost,
        "ftp-type": ftpType
      }
    });
  }
};

export default ApiService;
