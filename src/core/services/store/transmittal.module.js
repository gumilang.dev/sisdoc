import ApiService from "../api.service";

export const GET_TRANSMITTAL = "getTransmittal";
export const SET_TRANSMITTAL = "setTransmittal";

// export const GET_DESIGN_TRANSMITTAL = "getDesignTransmittal";
// export const SET_DESIGN_TRANSMITTAL = "setDesignTransmittal";

const state = {
    transmittalList: []
};

const getters = {
    getterTransmittal(state) {
        return state.transmittalList;
    }
};

const actions = {
    async [GET_TRANSMITTAL](context, param) {
        var code = param.project_code;
        await ApiService.query("project/transmittal", { project_code: code }).then((res) => {
            console.log(res.data.data);
            context.commit(SET_TRANSMITTAL, res.data.data);
        })
    },
    // async [GET_DESIGN_TRANSMITTAL](con   text) {
    //     await ApiService.get()
    // }
};

const mutations = {
    [SET_TRANSMITTAL](state, data) {
        state.transmittalList = data;
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};
