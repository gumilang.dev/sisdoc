// import ApiService from "../api.service";

// actions
export const GET_CHECKLIST_DISTRIBUTION = "getChecklistDistribution";
export const EDIT_CHECKLIST_DISTRIBUTION = "editChecklistDistribution";

// mutations
export const SET_CHECKLIST_DISTRIBUTION = "setChecklistDistribution";
export const SET_CHECKLIST_SEARCH = "setChecklistSearch";
export const SET_CHECKLIST_START = "setChecklistStart";
export const SET_CHECKLIST_END = "setChecklistEnd";
export const SET_CHECKLIST_ACTIVE = "setChecklistActive";
export const SET_CHECKLIST_TOTAL = "setChecklistTotal";

export const SET_EDIT_CHECKLIST = "setEditChecklist";
export const SET_EDIT_MASTER = "setEditMaster";
export const SET_EDIT_START = "startEditChecklist";
export const SET_EDIT_END = "endEditChecklist";
export const SET_EDIT_ACTIVE = "activeEditChecklist";
export const SET_EDIT_TOTAL = "totalEditChecklist";

const state = {
  listChecklist: [],
  masterSearchChecklist: [],
  startChecklist: "",
  endChecklist: "",
  activeChecklist: "",
  totalChecklist: "",
  listEditChecklist: [],
  masterEditChecklist: [],
  startEditChecklist: "",
  endEditChecklist: "",
  activeEditChecklist: "",
  totalEditChecklist: ""
};

const getters = {
  getterListChecklist(state) {
    return state.listChecklist;
  },

  getterSearchChecklist(state) {
    return state.masterSearchChecklist;
  },

  getterStartChecklist(state) {
    return state.startChecklist;
  },

  getterEndChecklist(state) {
    return state.endChecklist;
  },

  getterActiveChecklist(state) {
    return state.activeChecklist;
  },

  getterTotalChecklist(state) {
    return state.totalChecklist;
  },

  getterEditChecklist(state) {
    return state.listEditChecklist;
  },

  getterMasterEditChecklist(state) {
    return state.masterEditChecklist;
  },

  getterStartEditChecklist(state) {
    return state.startEditChecklist;
  },

  getterEndEditChecklist(state) {
    return state.endEditChecklist;
  },

  getterActiveEditChecklist(state) {
    return state.activeEditChecklist;
  },

  getterTotalEditChecklist(state) {
    return state.totalEditChecklist;
  }
};

const actions = {
  [GET_CHECKLIST_DISTRIBUTION]({ commit }, params) {
    var data = [
      {
        code: "LR98123",
        name: "PEMBANGUNAN SISTEM PERSINYALAN",
        status: "Open",
        owner: "BALAI TEKNIK PERKERETAPIAN"
      },
      {
        code: "LR123123",
        name: "PEMBUATAN SISTEM WEBSITE",
        status: "Open",
        owner: "RUMAH IT BANDUNG"
      }
    ];

    var start = params.start;
    var end = params.end;
    var active = params.activePage;

    var result = data.slice(start, end);

    commit(SET_CHECKLIST_DISTRIBUTION, result);
    commit(SET_CHECKLIST_SEARCH, data);
    commit(SET_CHECKLIST_START, start);
    commit(SET_CHECKLIST_END, end);
    commit(SET_CHECKLIST_ACTIVE, active);
    commit(SET_CHECKLIST_TOTAL, data.length);
  },

  async [EDIT_CHECKLIST_DISTRIBUTION]({ commit }, params) {
    var typeDoc;
    if (params.type == "DOC") {
      typeDoc = "Document";
    } else if (params.type == "DRW") {
      typeDoc = "Drawing";
    } else {
      typeDoc = "Form";
    }

    var data = [
      {
        no: 1,
        numbering: "TAN-D46-10100",
        title: "Signalling Layout Plan",
        station: "Tanate Rilau",
        type: typeDoc,
        scope: "Document"
      },
      {
        no: 1,
        numbering: "REG-D01-2000",
        title: "Edit Web Application",
        station: "Regular",
        type: typeDoc,
        scope: "Document"
      }
    ];

    var start = params.start;
    var end = params.end;
    var active = params.activePage;
    var total = data.length;
    var result = data.slice(start, end);

    commit(SET_EDIT_CHECKLIST, result);
    commit(SET_EDIT_MASTER, result);
    commit(SET_EDIT_START, start);
    commit(SET_EDIT_END, end);
    commit(SET_EDIT_ACTIVE, active);
    commit(SET_EDIT_TOTAL, total);
  }
};

const mutations = {
  [SET_CHECKLIST_DISTRIBUTION](state, data) {
    state.listChecklist = data;
    state.masterSearchChecklist = data;
  },

  [SET_CHECKLIST_START](state, data) {
    state.startChecklist = data;
  },

  [SET_CHECKLIST_END](state, data) {
    state.endChecklist = data;
  },

  [SET_CHECKLIST_ACTIVE](state, data) {
    state.activeChecklist = data;
  },

  [SET_CHECKLIST_TOTAL](state, data) {
    state.totalChecklist = data;
  },

  [SET_EDIT_CHECKLIST](state, data) {
    state.listEditChecklist = data;
  },

  [SET_EDIT_MASTER](state, data) {
    state.masterEditChecklist = data;
  },

  [SET_EDIT_START](state, data) {
    state.startEditChecklist = data;
  },

  [SET_EDIT_END](state, data) {
    state.endEditChecklist = data;
  },

  [SET_EDIT_ACTIVE](state, data) {
    state.activeEditChecklist = data;
  },

  [SET_EDIT_TOTAL](state, data) {
    state.totalEditChecklist = data;
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
