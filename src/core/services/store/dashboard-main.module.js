import ApiService from "../api.service";
export const GET_COUNTER_PROJECT="getCounterProject";
export const GET_COUNTER_DMP="getCounterDMP";
export const GET_COUNTER_MOM="getCounterMOM";
export const GET_COUNTER_CR="getCounterCR";
export const GET_COUNTER_PLAN="getCounterPlan";

export const GET_PROGRESS_OVERVIEW ="getProgressOverview";

export const SET_ERROR = "setError";
export const SET_COUNTER_PROJECT="setCounterProject";
export const SET_COUNTER_DMP="setCounterDMP";
export const SET_COUNTER_MOM="setCounterMOM";
export const SET_COUNTER_CR="setCounterCR";
export const SET_COUNTER_PLAN="setCounterPlan";

export const SET_LOADING_COUNTER_PROJECT="setLoadingCounterProject";

const state = {
  CounterProject: {},
  CounterDMP:{},
  CounterMOM:{},
  CounterCR:{},
  CounterPlan:{},
  errors: {},
  loading: false
};

const getters = {
currentCounterProject(state) {
    return state.CounterProject?.data
  },

  currentCounterDMP(state) {
    return state.CounterDMP?.data
  },

  currentCounterMOM(state) {
    return state.CounterMOM?.data
  },

   currentCounterCR(state) {
    return state.CounterCR?.data
  },

   currentCounterPlan(state) {
    return state.CounterPlan?.data
  },


};

const actions = {
  [GET_COUNTER_PROJECT](context, params) {
    return new Promise(resolve => {
      context.commit(SET_LOADING_COUNTER_PROJECT, true);
      ApiService.query(`counter/project`,params
      ).then(({data}) => {
        context.commit(SET_COUNTER_PROJECT, data);
        resolve(data);
        context.commit(SET_LOADING_COUNTER_PROJECT, false);
      })
      .catch(response => {
        context.commit(SET_ERROR, response?.data?.errors);
        context.commit(SET_LOADING_COUNTER_PROJECT, false);
      });
    });
  },
  [GET_COUNTER_DMP](context, params) {
    return new Promise(resolve => {
      context.commit(SET_LOADING_COUNTER_PROJECT, true);
      ApiService.query(`counter/dmp`,params
      ).then(({data}) => {
        context.commit(SET_COUNTER_DMP, data);
        resolve(data);
        context.commit(SET_LOADING_COUNTER_PROJECT, false);
      })
      .catch(response => {
        context.commit(SET_ERROR, response?.data?.errors);
        context.commit(SET_LOADING_COUNTER_PROJECT, false);
      });
    });
  },

  [GET_COUNTER_MOM](context, params) {
    return new Promise(resolve => {
      context.commit(SET_LOADING_COUNTER_PROJECT, true);
      ApiService.query(`counter/mom`,params
      ).then(({data}) => {
        context.commit(SET_COUNTER_MOM, data);
        resolve(data);
        context.commit(SET_LOADING_COUNTER_PROJECT, false);
      })
      .catch(response => {
        context.commit(SET_ERROR, response?.data?.errors);
        context.commit(SET_LOADING_COUNTER_PROJECT, false);
      });
    });
  },

   [GET_COUNTER_CR](context, params) {
    return new Promise(resolve => {
      context.commit(SET_LOADING_COUNTER_PROJECT, true);
      ApiService.query(`counter/cr`,params
      ).then(({data}) => {
        context.commit(SET_COUNTER_CR, data);
        resolve(data);
        context.commit(SET_LOADING_COUNTER_PROJECT, false);
      })
      .catch(response => {
        context.commit(SET_ERROR, response?.data?.errors);
        context.commit(SET_LOADING_COUNTER_PROJECT, false);
      });
    });
  },

  [GET_COUNTER_PLAN](context, params) {
    return new Promise(resolve => {
      context.commit(SET_LOADING_COUNTER_PROJECT, true);
      ApiService.query(`counter/plan`,params
      ).then(({data}) => {
        context.commit(SET_COUNTER_PLAN, data);
        resolve(data);
        context.commit(SET_LOADING_COUNTER_PROJECT, false);
      })
      .catch(response => {
        context.commit(SET_ERROR, response?.data?.errors);
        context.commit(SET_LOADING_COUNTER_PROJECT, false);
      });
    });
  },
};

const mutations = {
  [SET_COUNTER_PROJECT](state, data) {
    state.CounterProject = data;
  },

  [SET_COUNTER_DMP](state, data) {
    state.CounterDMP = data;
  },

  [SET_COUNTER_MOM](state, data) {
    state.CounterMOM = data;
  },

   [SET_COUNTER_CR](state, data) {
    state.CounterCR = data;
  },

   [SET_COUNTER_PLAN](state, data) {
    state.CounterPlan = data;
  },

  [SET_LOADING_COUNTER_PROJECT](state, loading) {
    state.loading = loading;
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
