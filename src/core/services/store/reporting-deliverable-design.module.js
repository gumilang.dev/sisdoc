import ApiService from "../api.service";
import { showToast } from "../../helper/toast.helper";

export const GET_DELIVERABLE_DESIGN = "getDeliverableDesign";

export const SET_ERROR = "setError";
export const SET_DELIVERABLE_DESIGN = "setDeliverableDesign";
export const SET_LOADING_DELIVERABLE_DESIGN = "setLoadingDeliverableDesign";

const state = {
  deliverableDesigns: {},
  errors: {},
  loading: false
};

const actions = {
  [GET_DELIVERABLE_DESIGN](context, params) {
    return new Promise(resolve => {
      context.commit(SET_LOADING_DELIVERABLE_DESIGN, true);

      ApiService.query(
        `report/deliverable/design/${params?.code}/project/${params?.latest}/latest`,
        params
      )
        .then(response => {
          context.commit(SET_DELIVERABLE_DESIGN, response?.data);

          resolve(response?.data);

          context.commit(SET_LOADING_DELIVERABLE_DESIGN, false);

          showToast("Sukses", "Berhasil mengambil data");
        })
        .catch(response => {
          context.commit(SET_ERROR, response?.data?.errors);

          context.commit(SET_LOADING_DELIVERABLE_DESIGN, false);

          showToast("Gagal", "Gagal mengambil data", "danger");
        });
    });
  }
};

const mutations = {
  [SET_DELIVERABLE_DESIGN](state, deliverableDesigns) {
    state.deliverableDesigns = deliverableDesigns;
  },

  [SET_LOADING_DELIVERABLE_DESIGN](state, loading) {
    state.loading = loading;
  }
};

export default {
  state,
  actions,
  mutations
};
