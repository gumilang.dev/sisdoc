import ApiService from "../api.service";

export const GET_PIC = "getPic";
export const GET_PIC_BY_PROJECT_ID = "getPicByProjectId";
export const GET_LIST_PIC = "getListPic";
export const DELETE_PIC = "deletePic";
export const SEARCH_PIC = "searchPic";

export const SHOW_PIC = "showPics";
export const SAVE_PIC = "savePic";
export const EDIT_PIC = "editPic";
export const SET_PAGE = "setPage";
export const SET_TOTAL_PAGINATION = "totalPagination";

export const SET_ERROR = "setError";
export const SET_PIC = "setPic";
export const STATUS_SAVE = "statusSave";

const state = {
  pics: [],
  listPic: [],
  statusSave: [],
  activePage: "",
  startPagination: "",
  endPagination: "",
  totalPagination: "",
  errors: {}
};

const getters = {
  currentPics(state) {
    return state.pics.data;
  },

  getterPic(state) {
    return state.listPic;
  },

  getterSave(state) {
    return state.statusSave;
  },

  getterPagination(state) {
    return state.activePage;
  },

  getterStartPage(state) {
    return state.startPagination;
  },

  getterEndPage(state) {
    return state.endPagination;
  },

  getterTotalPagination(state) {
    return state.totalPagination;
  }
};

const actions = {
  [GET_PIC](context, params) {
    return new Promise(resolve => {
      ApiService.query("pic", params)
        .then(({ data }) => {
          resolve(data);
          context.commit(SET_PIC, data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },

  async [GET_LIST_PIC]({ commit }, page) {
    await ApiService.query("pic").then(res => {
      var start = page[0].start;
      var end = page[0].end;
      var totalData = res.data.data.length;
      var totalPage = Math.ceil(totalData / 10);
      commit(SET_PAGE, page);
      commit(SHOW_PIC, res.data.data.slice(start, end));
      commit(SET_TOTAL_PAGINATION, totalPage);
    });
  },

  async [SAVE_PIC](context, data) {
    await ApiService.post("pic", data).then(res => {
      context.commit(STATUS_SAVE, res.data.code);
    });
  },

  async [EDIT_PIC](context, params) {
    ApiService.setHeader();
    await ApiService.put("pic", params).then(res => {
      console.log(res.data.data);
      console.log(context);
    });
  },

  [SEARCH_PIC](params) {
    ApiService.query("api", params).then(res => {
      console.log(res);
    });
  }
};

const mutations = {
  [SET_PIC](state, pics) {
    state.pics = pics;
  },

  [SET_ERROR](state, error) {
    state.errors = error;
  },

  [SHOW_PIC](state, data) {
    state.listPic = data;
  },

  [STATUS_SAVE](state, data) {
    state.statusSave = data;
  },

  [SET_PAGE](state, data) {
    var start = data[0].start;
    var end = data[0].end;

    state.activePage = data[0].page;
    state.startPagination = start;
    state.endPagination = end;
  },

  [SET_TOTAL_PAGINATION](state, data) {
    state.totalPagination = data;
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
