import ApiService from "../api.service";

export const GET_MENU = "getMenu";

export const SET_ERROR = "setError";
export const SET_MENU = "setMenu";

const state = {
  menus: [],
  errors: {}
};

const getters = {
  currentMenus(state) {
    return state.menus.data;
  }
};

const actions = {
  [GET_MENU](context, payload) {
    return new Promise(resolve => {
      ApiService.setHeaderWithContent();
      ApiService.query("menu", payload)
        .then(({ data }) => {
          context.commit(SET_MENU, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
          //console.log(response)
        });
    });
  }
};

const mutations = {
  [SET_MENU](state, menus) {
    state.menus = menus;
    state.errors = {};
  },

  [SET_ERROR](state, error) {
    state.errors = error;
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
