// action types
import ApiService from "../api.service";
import { showToast } from "../../helper/toast.helper";

export const GET_DESIGN_MANAGEMENT_PLAN = "getDesignManagementPlan";
export const GET_ONE_DESIGN_MANAGEMENT_PLAN_BY_CODE =
  "getOneDesignManagementPlanByCode";
export const UPDATE_DESIGN_MANAGEMENT_PLAN = "updateDesignManagementPlan";
export const UPDATE_DRAFT_DESIGN_MANAGEMENT_PLAN =
  "updateDraftDesignManagementPlan";
export const UPDATE_REVISION_DESIGN_MANAGEMENT_PLAN =
  "updateRevisionDesignManagementPlan";

// mutation types
export const SET_DESIGN_MANAGEMENT_PLAN = "setDesignManagementPlan";
export const SET_CURRENT_EDIT_DESIGN_MANAGEMENT_PLAN =
  "setCurrentDesignManagementPlan";
export const SET_LOADING_DESIGN_MANAGEMENT_PLAN =
  "setLoadingDesignManagementPlan";
export const SET_ERROR_DESIGN_MANAGEMENT_PLAN = "setErrorDesignManagementPlan";

const state = {
  loading: false,
  errors: {},
  designManagementPlan: {},
  currentDesignManagementPlan: null
};

const actions = {
  /**
   * Add breadcrumb
   * @param context
   * @param params
   */
  [GET_DESIGN_MANAGEMENT_PLAN](context, params) {
    return new Promise((resolve, reject) => {
      context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, true);

      ApiService.setHeader();

      ApiService.query(
        `project/${params.params.job_desk}/design/management/plan`,
        params
      )
        .then(({ data }) => {
          resolve(data);

          context.commit(SET_DESIGN_MANAGEMENT_PLAN, data);

          context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, false);

          showToast("Notifikasi Sukses", "Berhasil memuat data");
        })
        .catch(({ response }) => {
          reject(response);

          context.commit(
            SET_ERROR_DESIGN_MANAGEMENT_PLAN,
            response.data.errors
          );

          context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, false);

          showToast("Notifikasi Gagal", "Gagal memuat data", "danger");
        });
    });
  },

  [UPDATE_DESIGN_MANAGEMENT_PLAN](context, params) {
    return new Promise((resolve, reject) => {
      context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, true);

      ApiService.setHeader();

      ApiService.post("project/design/management/plan/fix", params)
        .then(({ data }) => {
          resolve(data);

          context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, false);

          showToast("Notifikasi Sukses", "Berhasil memuat data");
        })
        .catch(({ response }) => {
          reject(response);

          context.commit(
            SET_ERROR_DESIGN_MANAGEMENT_PLAN,
            response.data.errors
          );

          context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, false);

          showToast("Notifikasi Gagal", "Gagal memuat data", "danger");
        });
    });
  },

  [UPDATE_DRAFT_DESIGN_MANAGEMENT_PLAN](context, params) {
    return new Promise((resolve, reject) => {
      context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, true);

      ApiService.setHeader();

      ApiService.post("project/design/management/plan/draft", params)
        .then(({ data }) => {
          resolve(data);

          context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, false);

          showToast("Notifikasi Sukses", "Berhasil memuat data");
        })
        .catch(({ response }) => {
          reject(response);

          context.commit(
            SET_ERROR_DESIGN_MANAGEMENT_PLAN,
            response.data.errors
          );

          context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, false);

          showToast("Notifikasi Gagal", "Gagal memuat data", "danger");
        });
    });
  },

  [UPDATE_REVISION_DESIGN_MANAGEMENT_PLAN](context, params) {
    return new Promise((resolve, reject) => {
      context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, true);

      ApiService.setHeader();

      ApiService.post("project/design/management/plan/revision", params)
        .then(({ data }) => {
          resolve(data);

          context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, false);

          showToast("Notifikasi Sukses", "Berhasil memuat data");
        })
        .catch(({ response }) => {
          reject(response);

          context.commit(
            SET_ERROR_DESIGN_MANAGEMENT_PLAN,
            response.data.errors
          );

          context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, false);

          showToast("Notifikasi Gagal", "Gagal memuat data", "danger");
        });
    });
  },

  [GET_ONE_DESIGN_MANAGEMENT_PLAN_BY_CODE](context, params) {
    context.commit(SET_LOADING_DESIGN_MANAGEMENT_PLAN, true);

    context.commit(SET_CURRENT_EDIT_DESIGN_MANAGEMENT_PLAN, params?.code);
  }
};

const mutations = {
  [SET_DESIGN_MANAGEMENT_PLAN](state, data) {
    state.designManagementPlan = data;
  },

  [SET_LOADING_DESIGN_MANAGEMENT_PLAN](state, loading) {
    state.loading = loading;
  },

  [SET_CURRENT_EDIT_DESIGN_MANAGEMENT_PLAN](state, code) {
    state.currentDesignManagementPlan = null;

    state.currentDesignManagementPlan = state.designManagementPlan?.data?.find(
      design => design?.code === code
    );

    state.loading = false;
  }
};

export default {
  state,
  actions,
  mutations
};
