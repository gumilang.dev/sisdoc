import ApiService from "../api.service";
import { showToast } from "../../helper/toast.helper";
// import { showToast } from "../../helper/toast.helper";

export const GET_DL_DESIGN = "getDLDesign";
export const POST_DL_DESIGN = "postDLDesign";
export const UPDATE_DL_DESIGN = "updateDLDesign";
export const DELETE_DL_DESIGN = "deleteDLDesign";

// actions
export const SHOW_PROJECT = "showProject";
export const GET_STATION = "getStation";
export const GET_SCOPE = "getScope";
export const GET_DESIGN = "getDesign";
export const SEARCH_DESIGN = "searchDesign";
export const SAVE_DESIGN = "saveDesign";
export const CURRENT_DESIGN_CODE = "currentDesignCode";

// mutations
export const SET_PROJECT = "setProject";
export const SET_STATION = "setStation";
export const SET_SCOPE = "setScope";
export const SET_DESIGN = "setDesign";
export const SET_SEARCH = "setSearch";
export const STATUS_SAVE = "statusSave";
export const SET_TYPE_DESIGN = "setTypeDesign";
export const SET_MASTER_DESIGN = "setMasterDesign";
export const SET_CURRENT_CODE = "setCurrentDesignCode";

export const GET_DETAIL_DL_DESIGN_LIST = "getDetailDLDesignList";

export const SET_ERROR = "setError";
export const SET_LOADING = "setLoading";
export const SET_DL_DESIGN = "setDLDesaign";
export const SET_DETAIL_DL_DESIGN_LIST = "setDetailDLDesignList";
export const SET_UPDATED_DL_DESIGN = "setUpdatedDLDesign";

export const REMOVE_DETAIL_DL_DESIGN_LIST = "removeDetailDLDesignList";

const state = {
  dldesign: [],
  detailDlDesignList: {
    data: {
      dld: [],
      project: {}
    },
    code: null,
    message: null
  },
  loading: false,
  listProject: [],
  listDesign: [],
  listStation: [],
  listScope: [],
  currentDesignCode: "",
  typeDesign: "",
  resultSave: "",
  errors: {}
};

const getters = {
  currentdldesign(state) {
    return state.design?.data;
  },

  getterProject(state) {
    return state.listProject;
  },

  getterStation(state) {
    return state.listStation;
  },

  getterScope(state) {
    return state.listScope;
  },

  getterDesign(state) {
    return state.listDesign;
  },

  getterCurrentCode(state) {
    return state.currentDesignCode;
  },

  getterMasterDesign(state) {
    return state.masterSearch;
  },

  getterResultSave(state) {
    return state.resultSave;
  },

  getterTypeDesign(state) {
    return state.typeDesign;
  }
};

const actions = {
  [GET_DL_DESIGN](context, params) {
    return new Promise(resolve => {
      ApiService.get(
        "project",
        `${params.id_project}/${params.type_design}/ddl`
      )
        .then(({ data }) => {
          context.commit(SET_DL_DESIGN, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },

  [POST_DL_DESIGN](context, param) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.post("project/dld/man/hour", param)
        .then(({ data }) => {
          context.commit(SET_DL_DESIGN, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },

  [GET_DETAIL_DL_DESIGN_LIST](context, params) {
    return new Promise((resolve, reject) => {
      context.commit(SET_LOADING, true);

      ApiService.setHeader();

      ApiService.query(
        `project/${params.project_id}/${params.type_design}/dld`,
        params
      )
        .then(({ data }) => {
          resolve(data);

          context.commit(SET_DETAIL_DL_DESIGN_LIST, data);

          context.commit(SET_LOADING, false);

          showToast("Notifikasi Sukses", "Berhasil memuat data");
        })
        .catch(({ response }) => {
          reject(response);

          context.commit(SET_ERROR, response.data.errors);

          context.commit(SET_LOADING, false);

          showToast("Notifikasi Gagal", "Gagal memuat data", "danger");
        });
    });
  },

  [DELETE_DL_DESIGN](context, params) {
    return new Promise(resolve => {
      context.commit(SET_LOADING, true);

      ApiService.setHeader();

      ApiService.delete("project/dld", params)
        .then(({ data }) => {
          resolve(data);

          context.commit(SET_LOADING, false);

          context.commit(REMOVE_DETAIL_DL_DESIGN_LIST, params);

          showToast("Notifikasi Sukses", "Berhasil menghapus data");
        })
        .catch(({ response }) => {
          context.commit(SET_LOADING, false);

          context.commit(SET_ERROR, response.data.message);

          showToast("Notifikasi Gagal", "Gagal menghapus data", "danger");
        });
    });
  },

  [UPDATE_DL_DESIGN](context, body) {
    return new Promise(resolve => {
      context.commit(SET_LOADING, true);

      ApiService.setHeader();

      ApiService.post("project/dld/detail", body)
        .then(({ data }) => {
          resolve(data);

          context.commit(SET_LOADING, false);

          context.commit(SET_UPDATED_DL_DESIGN, body);

          showToast("Notifikasi Sukses", "Berhasil mengubah data");
        })
        .catch(({ response }) => {
          context.commit(SET_LOADING, false);

          context.commit(SET_ERROR, response);

          showToast("Notifikasi Gagal", "Gagal mengubah data", "danger");
        });
    });
  },

  [SHOW_PROJECT]({ commit }) {
    ApiService.query("project/design/jobdesk").then(res => {
      commit(SET_PROJECT, res.data.data);
    });
  },

  [GET_STATION]({ commit }, data) {
    ApiService.query("station/" + data.id + "/project").then(res => {
      commit(SET_STATION, res.data.data);
    });
  },

  [GET_SCOPE]({ commit }) {
    ApiService.query("scope").then(res => {
      commit(SET_SCOPE, res.data.data);
    });
  },

  [CURRENT_DESIGN_CODE]({ commit }, data) {
    commit(SET_CURRENT_CODE, data);
  },

  async [GET_DESIGN]({ commit }, parameter) {
    var send = {
      params: {
        id_project: parameter.code
      }
    };
    var url = "design/" + parameter.type + "/type";
    ApiService.setHeader();

    await ApiService.query(url, send).then(res => {
      console.log(res.data.data);
      console.log(parameter);
      commit(SET_DESIGN, res.data.data);
      commit(SET_MASTER_DESIGN, res.data.data);
      commit(SET_TYPE_DESIGN, parameter.type);
    });
  },

  [SEARCH_DESIGN]({ commit }, data) {
    commit(SET_SEARCH, data);
  },

  async [SAVE_DESIGN]({ commit }, data) {
    await ApiService.post("project/dld", data).then(res => {
      console.log(res.data.data);
      commit(STATUS_SAVE, { message: "success" });
    });
  }
};

const mutations = {
  [SET_DL_DESIGN](state, dldesign) {
    state.dldesign = dldesign;
  },

  [SET_ERROR](state, error) {
    state.errors = error;
  },

  [SET_DETAIL_DL_DESIGN_LIST](state, detailDlDesignList) {
    state.detailDlDesignList = detailDlDesignList;
  },

  [SET_LOADING](state, loading) {
    state.loading = loading;
  },

  [REMOVE_DETAIL_DL_DESIGN_LIST](state, params) {
    params.data.designs.map(id => {
      const index = state.detailDlDesignList.data?.dld.findIndex(
        dlDesign => dlDesign?.id === id
      );

      if (index >= 0) {
        state.detailDlDesignList.data?.dld.splice(index, 1);
      }
    });
  },

  [SET_UPDATED_DL_DESIGN](state, body) {
    body.dld.map(data => {
      const index = state.detailDlDesignList.data?.dld.findIndex(
        dlDesign => dlDesign?.id === data.id
      );

      if (index >= 0) {
        Object.assign(state.detailDlDesignList.data?.dld[index], {
          title: data.title,
          revision_version: data.revision_version,
          version: data.version,
          date_receive: data.date_receive,
          delay: data.delay,
          doc_room: data.doc_room,
          status: data.last_status.name,
          server: data.server,
          monitoring_asbuilt_status: data.monitoring_asbuilt_status,
          return_item: data.return_item,
          outstanding: {
            name: data.outstanding.drafter,
            code: data.outstanding.code
          }
        });
      }
    });
  },

  [SET_PROJECT](state, data) {
    state.listProject = data;
  },

  [SET_STATION](state, data) {
    state.listStation = data;
  },

  [SET_SCOPE](state, data) {
    state.listScope = data;
  },

  [SET_CURRENT_CODE](state, data) {
    state.currentDesignCode = data.code;
  },

  [SET_DESIGN](state, data) {
    state.listDesign = data;
  },

  [SET_TYPE_DESIGN](state, data) {
    state.typeDesign = data;
  },

  [SET_SEARCH](state, data) {
    state.listDesign = data;
  },

  [SET_MASTER_DESIGN](state, data) {
    state.masterSearch = data;
  },

  [STATUS_SAVE](state, data) {
    state.resultSave = data;
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
