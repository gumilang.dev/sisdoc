import ApiService from "../api.service";


export const CLEAN_STATE = "cleanState";
export const GET_LETTERING = "getLettering";
export const POST_LETTERING = "postLettering";
export const PUT_LETTERING = "putLettering"

export const SET_ERROR = "setError";
export const SET_LOADING = "setLoading";
export const SET_LETTERING = "setLettering";
export const SET_RESULT_POST_LETTERING = "setResultPostLettering";
export const SET_RESULT_PUT_LETTERING = "setResultPutLettering";

const state = {
 lettering: {
    data: [],
    code: null,
    message: null
  },
   resultPostLettering:{
    data: [],
    code: null,
    message: null
  },
   resultPutLettering:{
    data: [],
    code: null,
    message: null
  },
  loading: false,
  errors: {}
};

const getters = {
currentLettering(state) {
    return state.lettering?.data
  },

};

const actions = {
  [GET_LETTERING](context, params) {
    return new Promise((resolve, reject) => {
      context.commit(SET_LOADING, true);
      ApiService.setHeader();
      ApiService.query('letter',
        params
      )
      .then(({ data }) => {
        resolve(data);
        context.commit(SET_LETTERING, data);
        context.commit(SET_LOADING, false);
      })
      .catch(({ response }) => {
        reject(response);
        context.commit(SET_ERROR, response.data.errors);
        context.commit(SET_LOADING, false);
        
      });

      
    });
  },
  [POST_LETTERING](context, param) {
    return new Promise((resolve, reject) => {
      ApiService.setHeader();
      ApiService.post("letter", param)
        .then(({ data }) => {
          context.commit(SET_RESULT_POST_LETTERING, data);
          resolve(data);
        })
        .catch(({ response }) => {
            reject(response);
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },

   [PUT_LETTERING](context, param) {
    return new Promise((resolve, reject) => {
      ApiService.setHeader();
      ApiService.put("letter", param)
        .then(({ data }) => {
          context.commit(SET_RESULT_PUT_LETTERING, data);
          resolve(data);
        })
        .catch(({ response }) => {
            reject(response);
            context.commit(SET_ERROR, response.data.errors);
        });
    });
  },

};

const mutations = {


  [SET_ERROR](state, error) {
    state.errors = error;
  },

  [SET_LOADING](state, loading) {
    state.loading = loading;
  },

   [SET_LETTERING](state, data) {
    state.lettering = data;
  },

  [SET_RESULT_POST_LETTERING](state, data) {
    state.resultPostLettering = data;
  },

  [SET_RESULT_PUT_LETTERING](state, data) {
    state.resultPutLettering = data;
  },

};

export default {
  state,
  actions,
  mutations,
  getters,
};
