import { showToast } from "../../helper/toast.helper";
import ApiService from "../api.service";

export const GET_PROJECT_DETAIL = "getProjectDetail";
export const GET_PROJECT_DETAIL_BY_ID = "getProjectDetailByID";
export const POST_PROJECT_DETAIL = "postProjectDetail";
export const DELETE_PROJECT_STATION = "deleteProjectStation";
export const DELETE_PROJECT_PIC = "deleteProjectPIC";
export const DELETE_PROJECT_COMPANY = "deleteProjectCompany";
export const FILTER_PROJECT_DETAIL = "filterProjectDetail";

export const SET_ERROR = "setError";
export const SET_PROJECT_DETAILS = "setProjectDetails";
export const SET_PROJECT_DETAIL = "setProjectDetail";
export const SET_DATA_REMOVED = "setDataRemoved";

const initialState = {
  projectDetails: [],
  projectDetail: {
    project: {},
    project_station: [],
    project_company: [],
    project_docon: []
  },
  project_station: [],
  project_company: [],
  project_docon: [],
  project_data_removed: [],
  errors: {}
};

export const state = { ...initialState };

const getters = {
  currentProjectDetails(state) {
    return state.projectDetails.data;
  },
  currentProjectDetail(state) {
    return state.projectDetail;
  },
  projectStation(state) {
    return state.project_station;
  },
  projectCompany(state) {
    return state.project_company;
  },
  projectDocon(state) {
    return state.project_docon;
  },
  projectNicknameOptions(state) {
    var result = [{ value: null, text: "Pilih Nickname" }];

    if (state.projectDetails !== undefined) {
      state.projectDetails?.data?.map(project => {
        result.push({
          value: project.nickname,
          text: project.nickname
        });
      });
    }

    return result;
  }
};

const actions = {
  [FILTER_PROJECT_DETAIL](context, params) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.query(`project/${params.jobdesk}/jobdesk`, params)
        .then(({ data }) => {
          context.commit(SET_PROJECT_DETAILS, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },

  [GET_PROJECT_DETAIL](context, params) {
    return new Promise(resolve => {
      ApiService.setHeader();

      const jobDesk = typeof params === "object" ? params?.jobdesk : params;

      ApiService.get("project", `${jobDesk}/jobdesk`)
        .then(({ data }) => {
          context.commit(SET_PROJECT_DETAILS, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },
  [GET_PROJECT_DETAIL_BY_ID](context, params) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.get("project", `${params}/detail`)
        .then(({ data }) => {
          context.commit(SET_PROJECT_DETAIL, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },

  [POST_PROJECT_DETAIL](context, param) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.post("project/save", param)
        .then(({ data }) => {
          context.commit(SET_PROJECT_DETAIL, data);
          resolve(data);

          showToast("Success", "Berhasil menyimpan data.");
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);

          showToast("Gagal", "Gagal menyimpan data.", "danger");
        });
    });
  },
  [DELETE_PROJECT_STATION](context, params) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.delete("project/station", { data: params })
        .then(({ data }) => {
          context.commit(SET_DATA_REMOVED, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },
  [DELETE_PROJECT_PIC](context, params) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.delete("project/pic", { data: params })
        .then(({ data }) => {
          context.commit(SET_DATA_REMOVED, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },

  [DELETE_PROJECT_COMPANY](context, params) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.delete("project/company", { data: params })
        .then(({ data }) => {
          context.commit(SET_DATA_REMOVED, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  }
};

const mutations = {
  [SET_PROJECT_DETAIL](state, projectDetail) {
    state.projectDetail = projectDetail.data;
    state.project_station = projectDetail.data.project_station.map(station => {
      return {
        id: station.id,
        code: station.station_code,
        name: ""
      };
    });
    state.project_company = projectDetail.data.project_company.map(company => {
      return {
        id: company.id,
        code: company.company_code,
        name: ""
      };
    });
    state.project_docon = projectDetail.data.project_docon.map(pic => {
      return {
        id: pic.id,
        code: pic.pic_code,
        name: "",
        jobdesk: pic.jobdesk
      };
    });

    state.errors = {};
  },

  [SET_PROJECT_DETAILS](state, projectDetails) {
    state.projectDetails = projectDetails;
    state.errors = {};
  },

  [SET_ERROR](state, error) {
    state.errors = error;
  },

  [SET_DATA_REMOVED](state, data) {
    state.project_data_removed = data;
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
